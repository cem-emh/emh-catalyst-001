# Architecture

## What are the major services and components involved in the system?

### Dagster

[Dagster](https://docs.dagster.io/getting-started) is our main tool for
controlling the data pipeline. It manages the dependencies between different
data products, and gives us an interface to actually run the data pipeline.

When we write data processing code, we need to register it with Dagster before
it can help us execute the code. We cover that in a little more detail in the
[data integration guide](./data-integration.md).

Locally, we run Dagster through Docker Compose - along with development
versions of the other services.

In production, we run Dagster in a Docker container on a VM.

### Swift

[Swift] is the technology we use for storing the raw data files (see the
[landing zone documentation](#landing-zone) below for more info). It exposes a
REST API, which we interact with through the
`emh_catalyst_001.utils.SwiftEngine` class.

Locally, the Docker Compose file includes a Swift instance. *Unfortunately, it
doesn't persist files between container runs!* For now, the archival process is
fast enough that we run it every time we run an integration test.

Thankfully, we don't have to administer our own Swift cluster in production. We
have access to one in the Digital Alliance for Canada cloud resources.

### MySQL

The CODERS database is stored on a MySQL database in production, so the
automated process writes to that MySQL database as well. We will have a
separate namespace to avoid affecting any of the existing manual process.

We run a local MySQL through Docker Compose, but in production we will connect
directly to the real database.

## How does data flow through the system?

![Dataflow diagram showing the flow of one dataset through the landing/raw/curated zones & their corresponding tables.](./dataflow.png)
This overall flow is very similar to the existing manual data flow for CODERS.
The specifics of where the assets are stored is different, due to the needs of the automation and to avoid errors in the automation affecting our manually compiled data.

### Landing zone

We programmatically pull data from the outside world to the *landing zone*.
It's stored in a bucket in the Digital Alliance for Canada object store. This
data is still in the original format it was published in.

When we archive data and save it to the landing zone, we store some
[metadata](./metadata.md#archive-level-metadata) about the process in the MySQL
database. We track the access time, as well as a version ID that changes every
time the data changes. This lets us keep track of which source data was used
for any downstream assets.

<!-- TODO make latest *actually* be a pointer instead of a new copy. -->
**Note** currently, the version ID changes every time we run the code.
Fortunately, we also have a special version ID, `"latest"`, that always points
at the latest run.


### Raw zone

Then, we read data from the landing zone, convert it to a standardized tabular
form (`pandas.Dataframe`), and store it in the *raw zone*. Right now, assets in
the raw zone are only persisted locally to the Dagster worker, as pickled
Python objects.

Data in the raw zone is associated with the specific archive version it was drawn from, but otherwise unchanged.

### Curated zone

Finally, we take the data from the raw zone and combine similar data into assets in the curated zone. For example, whereas in the raw zone the Newfoundland and Quebec demand data from StatCan's HFED would be separate assets, in the curated zone they would be concatenated into one. Additionally, we associate the data with quality and data source metadata, and lowercase all of the columns for consistency.

These assets are persisted in the CODERS MySQL database.
When generating the curated zone assets, we store
[table-level](./metadata.md#table-level-metadata) and
[datapoint-level](./metadata.md#datapoint-level-metadata) metadata alongside it
in the MySQL database.

## What's the database schema?

![A diagram of the database schema.](./schema.png)

We store data in the database as many overlapping star schemas. Each curated dataset is represented as a fact table, which gets linked to several dimension tables by foreign key.

Many dimension tables are shared across many fact tables, such as the [archive](./metadata.md#archive-level-metadata)/[table](./metadata.md#table-level-metadata) metadata. [Datapoint](./metadata.md#datapoint-level-metadata) metadata is stored in additional columns in the fact table.

We use [Alembic](https://alembic.sqlalchemy.org/en/latest/tutorial.html) to
manage database migrations, and so there is also an `alembic_version` table.
See our [data integration
guide](./data-integration.md#how-do-i-update-the-database-schema-to-accommodate-my-new-dataset)
for more details about how we use Alembic.
