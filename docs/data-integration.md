# Data integration guide

## Overview

When integrating a new dataset, you will be bringing it through the **landing**, **raw**, and **curated** zones.

We'll go through these in order.


## Landing zone

### Writing the code

The first thing you have to do is figure out how to download the data you're looking for. For example, HFED has an 'api' that you can access using [`requests.get`](https://requests.readthedocs.io/en/latest/user/quickstart/). If you're just downloading files that are hosted somewhere you can also get them using `requests.get`. Some APIs might need you to pass in arguments as a JSON blob - in which case you'll likely need a `requests.post()` instead.

Whatever you land on, you need to add that logic to `landing/archivers/YOUR_DATA_SET_NAME_HERE.py`.

You can see an example at `landing/archivers/hfed.py` - you'll two key components there:

* an instance of `emh_catalyst_001.metadata.CODERSPackage`: this contains metadata that you want to store about the package (name, title, description, and so on)
* a class that inherits from `emh_catalyst_001.landing.classes.AbstractDatasetArchiver`: this contains the actual downloading logic.

For the `CODERSPackage`, you'll need to define a `name` and reference a `data_source_id`. The `name` will be used to connect the archive with the raw data.

If you are adding a new data source that you want to cite, you'll need to add it to `landing/assets.py`. There's a `data_sources` asset that you need to add your new data source to.

You should include at least:
* `id`: a short, unique name for this data source you can refer to elsewhere.
* `authors`: a comma-separated list of the authors you'd like to cite.
* `name`: the user-facing name of the data source - what you'd put in the citation.
* `description`: a longer description, if necessary.

There are other fields for adding the publication time, the DOI, the URL, and the specific part of the document you've accessed - see `data_source` in `schema.py` for details.

For the `AbstractDatasetArchiver` subclass, you need to include the package you just defined as the `package` class variable. You also need to implement the `get_resources(self)` method which should return a list of `ResourceInfo` objects. These `ResourceInfo` objects describe where something has been downloaded to, and what to call it. Given these `ResourceInfo`s and the `package`, methods in `AbstractDatasetArchiver` handle the metadata creation and file upload.

Here's an example:
```python
class YourArchiver(AbstractDatasetArchiver):
    package = CODERSPackage(...)

    def get_resources(self) -> list[ResourceInfo]:
        # download the files here
        # return a bunch of ResourceInfos
    ...
```

So long as you've created the archiver class in `landing/archivers` and inherited from `AbstractDatasetArchiver`, it should be automatically detected by dagster due to the logic in `emh_catalyst_001.landing.assets.create_archive_assets()`. But if for some reason you need to handle archival outside of the framework provided by the `AbstractDatasetArchiver`, you can always create an asset manually in `emh_catalyst_001.landing.assets` by using the `dagster.asset` decorator on a custom function.

## Raw zone

Once you have the data in the landing zone, you're ready to bring it into the raw zone - this means parsing the data into a Pandas dataframe. The raw zone assets aren't persisted anywhere - they're written to the local disk of the Dagster daemon. The data will actually be persisted again once we read these and turn them into curated assets.

This code should live in `raw/YOUR_DATASET_NAME.py`. Like above, you can look at `raw/hfed.py` for an example.

You'll see that there's a `GenericExtractor` subclass, which has a `package_name` and an `extract`. The `package_name` needs to match the `name` of the package you passed into the archiver.

Once you've defined an extractor class with a `package_name`, you'll need to define the `extract` method, which uses the archive ID and object name to grab the data from the landing zone.

It then parses the data into a `pandas.DataFrame` and adds the archive ID as the `archive_version` column of the DataFrame before returning it.

```python
class MyExtractor(GenericExtractor)
    package_name: "the package name you set earlier"

    def extract(self, archive_id: str, object_name: str) -> pd.DataFrame:
        # self.get_object_path(archive_id, object_name) will get you the path to the specific object in Swift
        # then use self.swift_engine.download(...) to get your object
        df = pd.read_csv(...)
        df["archive_version"] = archive_id
        return df
```

Once you have all that, you're ready to plug into Dagster.
Make a Dagster asset that instantiates your extractor and then runs it:

```python
@asset(required_resource_keys={"landing_zone_archive_versions"}
def raw_your_dataset(context: AssetExecutionContext) -> pd.DataFrame:
  se = SwiftEngine()
  archive_id = context.resources.landing_zone_archive_versions.YOUR_DATASET
  return MyExtractor(se).extract(archive_id, "your_object_name")
```

You'll notice that, to get the archive_id, you need to configure it via `context.resources.landing_zone_archive_versions`. To do that, you need to:

* you need to add your new dataset as an attribute of `emh_catalyst_001.raw.core.LandingZoneArchiveVersions`
* pin a version in `emh_catalyst_001/__init__.py:create_defs()`

Once all that is done, Dagster will pick up your asset, and you'll be able to materialize the asset through the Dagster UI. Next up is the curated zone.

*Note:* If you're parsing a bunch of assets that look similar, like in the case of HFED, you can also copy the asset factory pattern seen there - but if you're just looking at one asset, we don't need all that indirection.

## Curated zone

When moving data from the raw to the curated zone, we are doing a few things.
We may be *combining* multiple raw datasets,
we are *enriching* that data with source information and quality checks,
and we are *persisting* it all to MySQL.

This is also achieved through a Dagster asset - this time the code should live in `curated/YOUR_DATA_SET_NAME_HERE.py`.
You can see an example in `curated/hfed.py`.

Let's walk through a toy example. Here we have a curated dataset that relies on two raw datasets.

```python
@asset(io_manager_key="versioned_mysql_io_manager")
def curated_your_dataset(raw_your_dataset: pd.DataFrame, raw_other_dataset: pd.DataFrame):
    ...
```

* the `@asset` call is required to register this function with Dagster as before.
* to make sure we're actually persisting the data, we need to add the `io_manager_key="versioned_mysql_io_manager"`.
* to access the raw datasets in this function, you need to include them in the parameter list like we did here - Dagster will take care of wiring those values in as inputs.

Within the body of the asset, you can run whatever quality checks you'd like.

Once you've run the quality checks, you should add the results to `quality` column.
In this example, we have a simple static JSON value for each row,
but you may need to assign a `pd.Series` instead if you have different values in each row.

```python
output_dataset = output_dataset.assign(
  quality=json.dumps(
    {"some_variable_name":
      {
        "check_name": {"status": "passed"},
        "other_check_name": {
          "status": "error",
          "message": "Didn't pass this check."
        }
      }
    }))
```

You should follow that schema, of `{field name: {check_name: {status: string, message?: string}, check_name_2: {status: string, message?: string}}}`

<!-- TODO 2025-02-25: enforce that schema in the database. -->

Finally, we need to add a new table for us to persist the data to. This is defined in `schema.py` - see the `hfed_demand_fact` table for an example.

To do this, you'll need to add `sqlalchemy.Table`s to
`emh_catalyst_001.schema.metadata`. See the [SQLAlchemy MetaData
guide](https://docs.sqlalchemy.org/en/20/core/metadata.html) for details.

The new fact table should be named `<datasource>_<dataset>` - for example, demand data from HFED would be `hfed_demand`.

Once you've added these to the `MetaData`, you'll need to use `alembic` to make a database migration. The [Alembic documentation](https://alembic.sqlalchemy.org/en/latest/tutorial.html) has the details, but the rough steps are:


1. `export MYSQL_URL=mysql+mysqlconnector://coders-dev:example@localhost:3306/coders-dev`
1. `uv run alembic upgrade head` - this makes sure Alembic has your local database up
   to date
2. `uv run alembic revision --autogenerate -m "YOUR MESSAGE HERE"` - this
   automatically generates some database code to update the schema, inserting
   your new changes. It will live under `REPO_ROOT/alembic/versions/some_random_alphanumeric_string_plus_your_message.py`.
3. The migration will have two functions, `upgrade()` and `downgrade()`. You
   should verify that `upgrade()` makes the changes to the database schema that
   you'd like, and `downgrade()` undoes the changes in `upgrade`. If you need
   to do any special data processing/backfilling during the migration (say you
   added a non-nullable field to a table that already has data in it), then you
   should add that logic in `upgrade()`.
4. Run `uv run alembic upgrade head` to update your database schema to match the migrations you just made.
5. The migration won't be tracked in Git yet, so you'll have to manually `git add` it before committing.


## Connecting the production infrastructure

The default configuration assumes you're running everything using `docker compose` - this spins up a Dagster server, as well as the ancillary Swift and MySQL services. All the connection information defaults to these, which is handy in development.

But in production, you're going to have to point your Dagster application at the real persistence services.

To bring data into the landing zone, you will archive the data to our object storage. In our Digital Research Alliance for Canada project, that is a Swift cluster. The administrators of the CODERS infrastructure should have everything connected so you don't have to handle the details of the Swift setup. Just make sure that the `SWIFT_BASE_URL` environment variable is set to point at the Swift REST API, and that `SWIFT_USER`, `SWIFT_PASSWORD`, and `SWIFT_KEY` are all set correctly for authentication.

For persisting data in the curated zone, we use MySQL. Set the `MYSQL_URL` environment variable to point at the production MySQL server.
