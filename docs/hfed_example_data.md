# HFED example data

We take HFED demand data for Quebec and Newfoundland and store it in the CODERS MySQL database.


## Raw CSVs
The first step is to download the CSV files from HFED.

The first 10 lines of the Quebec data look like:

```csv
DATAFLOW,FREQ,REF_AREA,COUNTERPART_AREA,ENERGY_FLOWS,TIME_PERIOD,OBS_VALUE,DATETIME_LOCAL,DAYLIGHT_OFFSET,DATETIME_LOCAL_OFFSET,UNIT_MEASURE,UNIT_MULT,MEASURE_TYPE,OBS_STATUS,CONF_STATUS
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T22:00:00,15576,2021-10-06T00:00:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T22:15:00,15343,2021-10-06T00:15:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T22:30:00,15340,2021-10-06T00:30:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T22:45:00,15355,2021-10-06T00:45:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T23:00:00,15215,2021-10-06T01:00:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T23:15:00,14995,2021-10-06T01:15:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T23:30:00,14977,2021-10-06T01:30:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-05T23:45:00,15111,2021-10-06T01:45:00,1.0,4.0,MW,0,ENERGY,A,F
CCEI:DF_HFED_QC(1.0),N,CA_QC,_Z,DEMAND,2021-10-06T00:00:00,15010,2021-10-06T02:00:00,1.0,4.0,MW,0,ENERGY,A,F
```

And the first 10 lines of the Newfoundland data look like:

```csv
DATAFLOW,FREQ,REF_AREA,COUNTERPART_AREA,ENERGY_FLOWS,TIME_PERIOD,OBS_VALUE,DATETIME_LOCAL,DAYLIGHT_OFFSET,DATETIME_LOCAL_OFFSET,UNIT_MEASURE,UNIT_MULT,MEASURE_TYPE,OBS_STATUS,CONF_STATUS
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T19:30:00,917,2022-09-13T17:00:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T19:50:00,918,2022-09-13T17:20:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T19:55:00,922,2022-09-13T17:25:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T20:00:00,915,2022-09-13T17:30:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T20:05:00,910,2022-09-13T17:35:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T20:10:00,909,2022-09-13T17:40:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T20:40:00,893,2022-09-13T18:10:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T20:45:00,889,2022-09-13T18:15:00,1,2.5,MW,0,ENERGY,A,F
CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13T20:50:00,887,2022-09-13T18:20:00,1,2.5,MW,0,ENERGY,A,F
```

After we process it in `emh_catalyst_001.curated.hfed.hfed_demand`, it gets stored in several MySQL tables.

## `hfed_demand_fact`

This is the curated demand table - with both the Newfoundland and Quebec HFED data in one table, and with the following metadata attached:

* `quality`: JSON field describing quality metrics
* `archive_version_id`: field associating each row with a specific version of the data (note: currently, due to a bug, the `latest` version does not resolve to a specific hash on swift.)
* `data_source_id`: field associating each row with a data source, i.e. an organization that publishes data.
* `asset_version`: a unique ID for this *version* of the data. Each time we regenerate this table we'll have a new `asset_version`. This keys to the [`asset_version`](#asset_version) table which we'll describe below.

```csv
id,dataflow,freq,ref_area,counterpart_area,energy_flows,time_period,obs_value,datetime_local,daylight_offset,datetime_local_offset,unit_measure,unit_mult,measure_type,obs_status,conf_status,quality,archive_version_id,data_source_id,asset_version
1,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 19:30:00,917.0,2022-09-13 17:00:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
2,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 19:50:00,918.0,2022-09-13 17:20:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
3,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 19:55:00,922.0,2022-09-13 17:25:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
4,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:00:00,915.0,2022-09-13 17:30:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
5,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:05:00,910.0,2022-09-13 17:35:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
6,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:10:00,909.0,2022-09-13 17:40:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
7,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:40:00,893.0,2022-09-13 18:10:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
8,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:45:00,889.0,2022-09-13 18:15:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
9,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:50:00,887.0,2022-09-13 18:20:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
10,CCEI:DF_HFED_NL(1.0),N,CA_NL,_Z,DEMAND,2022-09-13 20:55:00,886.0,2022-09-13 18:25:00,1.0,2.5,MW,0.0,ENERGY,A,F,"{""consistency_checks"": ""No checks""}",latest,statcan,hfed_demand__bd6b0471-da81-43dd-8c7e-946fa09f9710
```

## `asset_version`

As mentioned in [`hfed_demand_fact`](#hfed_demand_fact) we track some information for each time we generate a table.

That looks like:

```csv
id,active,git_hash,asset_name,run_id,date_created,name,description,author,subject_area,data_format,access_conditions
0,hfed_demand__7bba1275-269c-4471-9fce-ad46148dcf80,True,70fe9bdc1397989823d8ad4efbd0045eeb6b404d,hfed_demand,7bba1275-269c-4471-9fce-ad46148dcf80,2024-11-07 00:21:31,hfed_demand,Hourly electricity demand data for Newfoundland and Quebec.,EMH,Electricity Demand,MySQL table,CODERS terms
1,hfed_demand__bba41d75-f02f-484e-babb-141ddad477a0,False,70fe9bdc1397989823d8ad4efbd0045eeb6b404d,hfed_demand,bba41d75-f02f-484e-babb-141ddad477a0,2024-11-07 00:15:14,hfed_demand,Hourly electricity demand data for Newfoundland and Quebec.,EMH,Electricity Demand,MySQL table,CODERS terms
```

This represents two instances of the asset. The fields mean:

* `active`: whether this is the active version of the asset - whenever we write
  a new version, we make that the active version. When we read an asset from
  the DB we only read the active version. But we keep the other versions around
  so we can look at historical versions of the data.
* `git_hash`: the git hash of the commit that generated this data
* `asset_name`: machine-facing name of this asset
* `run_id`: the Dagster run ID - foreign key to `coders_version.run_id`.
* `date_created`: the datetime that this asset was created.
* `name`: human-facing name of this asset
* `description`: human-facing description of asset
* `author`: name of the creator of the asset
* `subject_area`: free-form text field describing the subject of the asset
* `data_format`: free-form text field describing the data format
* `access_conditions`: free-form text field describing the rules which apply to accessing this data.

## `coders_version`

We associate a human-facing CODERS version (`YYYY.MM.DD.XX`, where `XX` is just the ordinal number of this run on the date `YYYY-MM-DD`) with each Dagster run ID, so we can better understand when a specific run was made and better cite a specific run.

```csv
,run_id,date_created,coders_version
0,5a17119b-d5c2-4e7d-b0e4-3625ae8e2752,2024-11-07 00:06:52,2024.11.07.00
1,7bba1275-269c-4471-9fce-ad46148dcf80,2024-11-07 00:14:45,2024.11.07.01
```

## `archive_metadata`

We also track information about each time we create a new archive of the raw data in `archive_metadata` - it looks like:


```csv
,archive_version,name,title,description,keywords,data_type,subject_area,release_date,doi,citation_information,update_frequency,source_quality
0,baedd4ac-286a-4915-a915-839188b40821,HFED,HFED,Raw HFED datasets.,"HFED, electricity, demand, load",Officially reported,Electricity,2024-11-07T00:10:44.565389,,MLA,Quarterly,HFED is a high-quality dataset that is officially reported by the Canadian Centre for Energy Information.
```

Most of these are self explanatory free-form text fields, but here are some fields of note:

* `archive_version`: this is the UUID that is associated with the archive in Swift.
* `keywords`: this is just a string that is split by `, `.
