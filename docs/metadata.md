# Metadata system

## Archive-level metadata

Whenever we download and archive data from the outside world, we need to track some metadata about it.

We think of each archive as a data package, based on the [Frictionless Data
Package standard](https://specs.frictionlessdata.io/data-package). A data
package then contains many data resources, also based on the [Frictionless Data
Resource standard](https://specs.frictionlessdata.io/data-resource/).

We represent data packages in the code with a subclass of the
official Frictionless class: `CODERSPackage`. Similarly we represent data resources with the `CODERSResource` class, which also inherits from its official Frictionless counterpart.

For each landing zone asset, we call `AbstractDatasetArchiver.execute()`. This
eventually calls `CODERSPackage.to_dataframes()` to generate a tabular version
of the `CODERSPackage`, which we then store in the `archive_metadata` table in
MySQL.

The most-up-to-date information on the specific fields can be found in the `archive_metadata` table definition within `src/emh_catalyst_001/schema.py`.


## Table-level metadata

<!-- TODO we only make the asset version now. Is that OK? Drop the table metadata. That should live in the asset version if we so choose. Still defined by the resource definition. -->

We store metadata that we expect to be shared by all the data in the table in the `asset_version` table.

The asset version contains operational metadata about this version of the table, and is stored in the `asset_version` table in MySQL. The `MySQLIOManager` generates and stores this metadata automatically for every asset that it persists.

Additionally, it contains information about the data itself. This corresponds
to the information within `CODERSResource` - author, subject area, data format,
access conditions, and more. You will need to define this table metadata in
`emh_catalyst_001.metadata.RESOURCES`, but once you do, it will be
automatically persisted by `MySQLIOManager`.

The most-up-to-date information on the specific fields we store can be found in
the `asset_version` table definition within
`src/emh_catalyst_001/schema.py`.

## Datapoint-level metadata


For our curated datasets, we want to associate each datapoint with quality metrics and the ultimate source of the data. We store these in the `quality` and `data_source_id` columns of the curated data.

`quality` is a JSON field, and we don't have a strict schema for it yet. One row may contain multiple different data points with differing quality metrics. The JSON format lets us compactly associate each row with multiple different sets of quality metrics.

`data_source_id` is a foreign key to the `data_source` table, which contains information about the different organizations we could be getting data from - who they are, what level of processing they apply to their data, etc. Some of these fields may vary on a dataset-by-dataset basis, or across time for the same dataset, in which case we will store them on the `asset_version` instead.
