# Running the ETL manually

The dagster instance has two [code locations](https://docs.dagster.io/concepts/code-locations), one for the landing zone and another for raw and and curated zones.

## Landing Zone
To populate the swift instance with raw data, go to the [`emh_coders_001`](http://localhost:3000/locations/emh_catalyst_001.landing/asset-groups/landing) and click the "Materialize" button. This will execute archivers which pull the raw data from the sources and archive it in the local swift instance. The data is archived by datasource. Each datasource will have multiple archives which are named with a unique identifier. Each archive will have a collection of raw datafiles and a single `datapackage.json`. The `datapackage.json` contains metadata about the archived data like the date created, the source, the authors and the licensing.

```
coders-archive
├── hfed
│   ├── <uuid_1>
│   │   ├── datapackage.json
│   │   ├── hfed_newfoundland_demand.csv
│   │   ├── hfed_quebec_demand.csv
│   │   └── ...
│   └── <uuid_2>
│       ├── datapackage.json
│       ├── hfed_newfoundland_demand.csv
│       ├── hfed_quebec_demand.csv
│       └── ...
└── another_source
    └── ...
```

## Raw Zone
The ETL process needs to know which unique identifier of the raw data to use. To view all of the archived data, go to the [`swift.ipynb`](http://127.0.0.1:8888/lab/workspaces/auto-s/tree/notebooks/swift.ipynb) notebook and run the cells. Grab the desired UUID for a data source and update the `archive_uuid` class variable in the data sources extractor. See the `emh-catalyst-001/src/emh_catalyst_001/raw/hfed.py` module for an example.

## Initializing the output database

We use [alembic](https://alembic.sqlalchemy.org/en/latest/) to apply changes to the database schema. Initialize the database by attaching to the dagster container

```
docker exec -it emh_catalyst-dagster-1 bash
```

then execute the alembic migrations

```
alembic upgrade head
```

If you make changes to the database schema in `src/emh_catalyst_001/metadata.py`, you need to generate a new migration file and apply it to the database. Be sure to add the new file to git!

```
alembic revision --autogenerate -m "Add my cool table"
alembic upgrade head
git add migrations
git commit -m "Migration: added my cool table"
```

## Curated Zone
Now that the data is archived and the database is set up we can execute the ETL. Go to the [`emh_coders_job`](http://localhost:3000/locations/emh_catalyst_001/jobs/coders_job) dagster job and click "Materialize". This will start a run that extracts the raw data, cleans it, adds metadata and loads it all to the MySQL database.

Run this command to view the tables in the MySQL database:

```
docker exec -it emh_catalyst-mysql-1 mysql -u root -p
```

See the `docker-compose.yml` file for the connection details.

Once you're in the mysql shell attached to the database and list the tables:

```
USE coders-dev;
SHOW TABLES;
```


## Troubleshooting

Sometimes, you end up with the database in a bad state. It happens!

Fortunately, it's easy to completely wipe everything and start over.

If you run `docker compose down -v` it will delete the MySQL data volume. When
you next run `docker compose up` it will create a fresh new volume for you!
