import json
import logging

from dagster import asset
import pandas as pd


logger = logging.getLogger(__name__)


@asset(io_manager_key="versioned_mysql_io_manager")
def hfed_demand(
    raw_hfed_newfoundland_demand: pd.DataFrame, raw_hfed_quebec_demand: pd.DataFrame
):
    logger.info("Starting processing for `hfed_demand`")
    # TODO: all of the provicial demand data might have the same structure so it might make more sense to combine it all during extraction
    logger.info("concating")
    demand = pd.concat(
        [raw_hfed_newfoundland_demand, raw_hfed_quebec_demand], ignore_index=True
    )
    demand.columns = demand.columns.str.lower()
    demand = demand.rename(columns={"archive_version": "archive_version_id"})
    # TODO 2025-01-02 set dtypes
    # TODO add asset checks to check primary key

    demand = demand.assign(
        quality=json.dumps({"demand": {"consistency_checks": "No checks"}}),
        data_source_id="statcan-hfed",
    )

    return demand
