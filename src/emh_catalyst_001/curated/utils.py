from typing import Annotated, Any
from pydantic import BaseModel, ConfigDict, Field
import pandas as pd
from datetime import date

Percentage = Annotated[
    str,
    Field(
        ge=0,
        le=1,
    ),
]


class ColumnMetadata(BaseModel):
    column_name: str
    archive_version: str = ""
    accuracy_rate: Percentage = None
    completeness_percentage: Percentage = None
    consistency_checks: str = None
    timeliness_score: int = None
    reliability_score: int = None
    quality_assessment_history_date: date = None
    quality_assessment_history_finding: str = None
    validation_process: str = None
    definition: str = None
    clarifications: str = None
    calculations: str = None
    limitations: str = None

    model_config = ConfigDict(arbitrary_types_allowed=True, extra="forbid")

    @classmethod
    def from_dict(cls, column_name, dct):
        return cls(column_name=column_name, **dct)

    def to_dataframe(
        self, fact_series: pd.Series, archive_version_series: pd.Series
    ) -> pd.DataFrame:
        """
        Create a metadata dataframe for a series of a fact table column.

        This method also adds the archive_version series to the metadata dataframe.

        The structure of the dataframe is:

        | index | field_name | metadata_field_name | metadata_field_type | metadata_value |

        Args:
            fact_series: The series of the fact table column.
            archive_version_series: The series of the archive_version column.
        Raises:
            ValueError: If the fact_series name does not match the column_name
            ValueError: If the index of the archive_version series does not match the index of the fact_series.
        Returns:
            A metadata dataframe for the fact table column.
        """
        if not archive_version_series.index.equals(fact_series.index):
            raise ValueError(
                "Index of the archive_version series does not match the index of the fact_series."
            )
        if fact_series.name != self.column_name:
            raise ValueError(
                f"The fact column name {fact_series.name} does not match column_name {self.column_name} specified in the metadata constructor."
            )

        metadata_df = []

        for field_name, field_obj in self.model_fields.items():
            # add metadata field if it is not None or if the ColumnMetadata attribute is not the column_name
            # We skip the metadata columns that are none because not all fact columns require all metadata fields
            if (field_name != "column_name") & (getattr(self, field_name) is not None):
                metadata_series = pd.DataFrame(index=fact_series.index)
                metadata_series["field_name"] = self.column_name
                metadata_series["metadata_field_name"] = field_name
                metadata_series["metadata_field_type"] = (
                    field_obj.annotation.__name__
                )  # TODO: Figure out what type it makes sense to map these python types to

                if field_name == "archive_version":
                    metadata_series["metadata_value"] = archive_version_series
                else:
                    metadata_series["metadata_value"] = getattr(self, field_name)

                metadata_df.append(metadata_series)

        return pd.concat(metadata_df)


class MetadataBuilder(BaseModel):
    """
    A class to construct metadata for a fact table.

    Attributes:
        column_metadata: A list of column metadata objects.

    """

    column_metadata: list[ColumnMetadata] = []

    model_config = ConfigDict(arbitrary_types_allowed=True, extra="forbid")

    @classmethod
    def from_dict(cls, dct: dict[str, dict[str, Any]]):
        """
        Create a CellMetadataConstructor object from a dictionary.

        Structure of the dictionary should be:
        {
            "fact_table_column_name": {
                "metadata_field_name": default_value,
                ...
            },
            ...
        }

        Args:
            dct: A dictionary containing column metadata.
        Returns:
            A CellMetadataConstructor object.
        """
        cell_metadata = [
            ColumnMetadata.from_dict(field_name, metadata_fields)
            for field_name, metadata_fields in dct.items()
        ]
        return cls(column_metadata=cell_metadata)

    def to_dataframe(self, fact_df: pd.DataFrame) -> pd.DataFrame:
        """
        Create a metadata dataframe for a fact table.

        Args:
            fact_df: The fact table dataframe.
        Returns:
            A metadata dataframe for the fact table.
            The structure of the dataframe is:

            | index | field_name | metadata_field_1 | metadata_field_2 | ... |
        """
        if "archive_version" not in fact_df.columns:
            raise ValueError("archive_version column missing in fact table.")
        metadata_df = []
        for df_field in self.column_metadata:
            if df_field.column_name not in fact_df.columns:
                raise ValueError(
                    f"{df_field.column_name} column missing in fact table but specified in metadata constructor."
                )
            metadata_df.append(
                df_field.to_dataframe(
                    fact_df[df_field.column_name], fact_df["archive_version"]
                )
            )
        return pd.concat(metadata_df)
