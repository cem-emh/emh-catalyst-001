import io
import json
import os
from pathlib import Path
import tempfile
from typing import Generator
from swiftclient.service import SwiftService, SwiftUploadObject
import logging
from pydantic import BaseModel, ConfigDict
import pandas as pd
from hashlib import md5


logger = logging.getLogger()


def get_coders_version() -> str:
    """Get the current coders version from the environment."""
    coders_version = os.environ.get("CODERS_VERSION", None)
    if coders_version is None:
        raise ValueError("CODERS_VERSION environment variable not set.")
    return coders_version


def create_uuid_from_values(values: list[str] | str | pd.Series) -> str:
    """Create a positive hash from a list of values."""
    if isinstance(values, str) or isinstance(values, list):
        values = json.dumps(values)
    elif isinstance(values, pd.Series):
        values = values.to_json()
    else:
        raise ValueError("values must be a list or a string to hash.")
    return md5(values.encode("utf-8")).hexdigest()


class VersionedTable(BaseModel):
    """A table which has a unique version ID associated.

    The main class for reading data *out* of the database.
    """

    version_id: str
    dataframe: pd.DataFrame

    model_config = ConfigDict(arbitrary_types_allowed=True)


class SwiftEngine:
    """Class for interacting with the archive Swift object store.

    Attributes:
        service: A SwiftService object that is used to interact with the
            Swift object store.
    """

    def __init__(self):
        """Constructs a SwiftEngine."""
        base_url = os.getenv("SWIFT_BASE_URL", "http://swift:8080").rstrip("/")
        user = os.getenv("SWIFT_USER", "test")
        password = os.getenv("SWIFT_PASSWORD", "tester")
        key = os.getenv("SWIFT_KEY", "testing")
        self.service = SwiftService(
            options={
                "auth_version": "1.0",
                "user": f"{user}:{password}",
                "key": f"{key}",
                "auth": f"{base_url}/auth/v1.0",
            }
        )

    def upload(self, container: str, objects: list[SwiftUploadObject]) -> None:
        """Uploads objects to the Swift object store.

        Args:
            container: The name of the container to upload the objects to.
            objects: A list of SwiftUploadObject objects to upload.
        """
        upload_responses = self.service.upload(container, objects)
        for upload_response in upload_responses:
            logger.debug(f"{upload_response=}")

    def download(
        self, container: str, objects: list[str]
    ) -> Generator[io.BytesIO, None, None]:
        """Downloads objects from the Swift object store.

        Args:
            container: The name of the container to download the objects from.
            objects: A list of object names to download.
        Yields:
            A generator that yields the downloaded objects as io.BytesIO objects.
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            results = self.service.download(
                container, objects, options={"out_directory": tmpdir}
            )
            for res in results:
                assert res[
                    "success"
                ], f"Failed to download {res['object']}\n {res['traceback']}"
                if (fp := Path(res["path"])).exists():
                    with fp.open("rb") as f:
                        yield f.read()


def get_mysql_url():
    """Read MySQL URL from env var, or default.

    Could get lumped into a full IOSettings class later.
    """
    return os.getenv(
        "MYSQL_URL", "mysql+mysqlconnector://coders-dev:example@mysql:3306/coders-dev"
    )
