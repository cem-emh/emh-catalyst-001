"""Construct the EMH Catalyst 001 pipeline."""

import itertools
import importlib.metadata

import pandas as pd
from dagster import (
    asset_check,
    Definitions,
    load_asset_checks_from_modules,
    load_assets_from_modules,
    define_asset_job,
)

from emh_catalyst_001 import raw, curated, io_managers
from emh_catalyst_001.raw.core import LandingZoneArchiveVersions
from emh_catalyst_001.utils import get_mysql_url

# If you are adding a new asset, make sure its module is in the appropriate dictionary below!

raw_module_groups = {
    "raw_hfed": [raw.hfed],
}

curated_module_groups = {
    "curated_hfed": [curated.hfed],
}

all_asset_modules = raw_module_groups | curated_module_groups

default_assets = list(
    itertools.chain.from_iterable(
        load_assets_from_modules(
            modules,
            group_name=group_name,
        )
        for group_name, modules in all_asset_modules.items()
    )
)


def make_non_empty_check(asset):
    @asset_check(asset=asset, blocking=True)
    def check_not_empty(result):
        if isinstance(result, pd.DataFrame):
            assert not result.empty

    return check_not_empty


all_assets_not_empty = [make_non_empty_check(asset) for asset in default_assets]

default_asset_checks = (
    list(
        itertools.chain.from_iterable(
            load_asset_checks_from_modules(
                modules,
            )
            for modules in all_asset_modules.values()
        )
    )
    + all_assets_not_empty
)


def create_defs():
    """Create all Dagster definitions.

    Pulled out into a function for testability.
    """
    return Definitions(
        jobs=[define_asset_job(name="coders_job")],
        assets=default_assets,
        asset_checks=default_asset_checks,
        resources={
            "versioned_mysql_io_manager": io_managers.VersionedMySQLIOManager(
                get_mysql_url()
            ),
            "landing_zone_archive_versions": LandingZoneArchiveVersions(
                hfed="latest"  # replace with a pinned version of the archive
            ),
        },
    )


defs = create_defs()

__author__ = "Energy Modeling Hub"
__contact__ = "pierre.cilpa@cme-emh.ca"
__maintainer__ = "Energy Modeling Hub"
# __license__ = "MIT License"
__version__ = importlib.metadata.version("emh_catalyst_001")
__description__ = "Tools for liberating Canadian electric utility data."
__projecturl__ = "https://cme-emh.ca/"
__downloadurl__ = "https://gitlab.com/cem-emh/emh-catalyst-001"
