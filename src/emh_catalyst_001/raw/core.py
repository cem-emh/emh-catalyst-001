"""Core classes that define interfaces for usage throughout the raw layer."""

from abc import ABC, abstractmethod

from dagster import ConfigurableResource
import pandas as pd

from emh_catalyst_001.utils import SwiftEngine


class LandingZoneArchiveVersions(ConfigurableResource):
    """Define the version strings we want read out of the landing zone."""

    hfed: str


class GenericExtractor(ABC):
    """
    Abstract class for extracting data from the coders archive.

    Attributes:
        archive_uuid: The UUID of the desired archive that the data is stored in.
            You'll have to look up the UUID in the coders archive when updating the data.
        package_name: The name of the package that the data is stored in
    """

    package_name: str

    def __init__(self, swift_engine: SwiftEngine | None) -> None:
        """Constructs the extractor."""
        if not swift_engine:
            self.swift_engine = SwiftEngine()
        self.swift_engine = swift_engine

    def get_object_path(self, archive_id: str, object_name: str) -> str:
        """Returns the full path to the object in the archive."""
        return f"{self.package_name}/{archive_id}/{object_name}"

    @abstractmethod
    def extract(self, object_name: str) -> pd.DataFrame:
        """Extract the data from the coders archive."""
        pass
