"""Module for extracting data from the HFED archive."""

from emh_catalyst_001.raw.core import GenericExtractor

import io
import logging

from dagster import asset, AssetExecutionContext

import pandas as pd

from emh_catalyst_001.utils import SwiftEngine

logger = logging.getLogger(__name__)


class HFEDExtractor(GenericExtractor):
    """Extracts data from the HFED archive."""

    package_name: str = "HFED"

    def extract(self, archive_id: str, object_name: str) -> pd.DataFrame:
        """Extracts an object from the HFED archive.

        Args:
            object_name: The name of the object to extract.
                The object_name should not include the file extension.
        Returns:
            The extracted data as a pandas DataFrame.
        """
        # TODO: get file extension using datapackages!!
        object = f"{self.get_object_path(archive_id, object_name)}.csv"
        objects = list(self.swift_engine.download("coders-archive", [object]))
        assert len(objects) == 1, "Swift download returned more than one object"
        df = pd.read_csv(io.BytesIO(objects[0]))
        df["archive_version"] = archive_id
        return df


def extract_hfed_asset_factory(asset_name: str):
    """Factory function for extracting HFED assets."""

    @asset(
        name=f"raw_{asset_name}",
        required_resource_keys={"landing_zone_archive_versions"},
    )
    def extract_hfed_asset(context: AssetExecutionContext) -> pd.DataFrame:
        """Extracts an HFED asset from the archive."""
        se = SwiftEngine()
        archive_id = context.resources.landing_zone_archive_versions.hfed
        logger.info(f"Starting extraction of {archive_id}")
        extractor = HFEDExtractor(se)
        return extractor.extract(archive_id, asset_name)

    return extract_hfed_asset


raw_hfed_assets = ["hfed_newfoundland_demand", "hfed_quebec_demand"]

extract_hfed_assets = [
    extract_hfed_asset_factory(asset_name) for asset_name in raw_hfed_assets
]
