"""Collect archiver assets."""

import importlib
import logging
import pkgutil

from dagster import asset, AssetExecutionContext, AssetsDefinition
import sqlalchemy as sa
from sqlalchemy.dialects.mysql import insert

from emh_catalyst_001.landing.classes import AbstractDatasetArchiver
from emh_catalyst_001 import schema
from emh_catalyst_001.utils import get_mysql_url

logger = logging.getLogger(__name__)


@asset()
def data_source():
    engine = sa.create_engine(get_mysql_url())
    data_sources = [
        {
            "id": "statcan-hfed",
            "authors": "Statistics Canada, Canadian Center for Energy Information",
            "name": "High-Frequency Electricity Data",
            "description": "A continuously-updated dataset containing near-real-time electricity data.",
        },
    ]
    with engine.begin() as con:
        base = insert(schema.data_source)
        for source in data_sources:
            con.execute(
                base.values(source).on_duplicate_key_update(
                    {colname: colname for colname in source if colname != "id"}
                )
            )


def get_archivers() -> list[AbstractDatasetArchiver]:
    """Dynamically import all archivers from the archivers subpackage."""
    archiver_classes = []

    # Package name and subpackage name
    package_name = "emh_catalyst_001.landing.archivers"

    # Get package path dynamically
    package_path = importlib.import_module(package_name).__path__

    # Iterate over modules in the subpackage
    for _, module_name, _ in pkgutil.walk_packages(
        path=package_path, prefix=package_name + "."
    ):
        # Import the module dynamically
        _ = importlib.import_module(module_name)

        archiver_classes += AbstractDatasetArchiver.__subclasses__()
    return archiver_classes


def archive_asset_factory(archive_class: AbstractDatasetArchiver):
    """Factory function for creating dagster assets for archivers."""

    @asset(name=archive_class.get_name(), deps=["data_source"])
    def archive_asset(context: AssetExecutionContext):
        """Execute an archiver."""
        archive_instance = archive_class()
        logger.info(f"Starting landing zone archival of {archive_instance}")
        archive_instance.execute()

    return archive_asset


def create_archive_assets() -> list[AssetsDefinition]:
    """Create dagster assets for all archivers."""
    archive_assets = []
    for archive_class in get_archivers():
        archive_assets.append(archive_asset_factory(archive_class))
    return archive_assets


archive_assets = create_archive_assets()
