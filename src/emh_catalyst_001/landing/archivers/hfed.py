"""HFED datasets archiver."""

import io
import os
import requests

import fsspec

from emh_catalyst_001.landing.classes import (
    AbstractDatasetArchiver,
    ResourceInfo,
)
from emh_catalyst_001.metadata import CODERSPackage

package = CODERSPackage(
    name="HFED",
    data_source_id="statcan-hfed",
)


class HFEDArchiver(AbstractDatasetArchiver):
    """Archiver for HFED datasets."""

    package = package
    base_url = "https://api.statcan.gc.ca/hfed-dehf/sdmx/rest/data/"
    resource_urls = {
        "hfed_newfoundland_demand": f"{base_url}CCEI,DF_HFED_NL,1.0/N...DEMAND?&dimensionAtObservation=AllDimensions&format=csv",
        "hfed_quebec_demand": f"{base_url}CCEI,DF_HFED_QC,1.0/N...DEMAND?&dimensionAtObservation=AllDimensions&format=csv",
        "hfed_prince_edward_island_load": f"{base_url}CCEI,DF_HFED_PE,1.0/N...ON_ISL_LOAD?&dimensionAtObservation=AllDimensions&format=csv",
    }

    def get_resources(self) -> list[ResourceInfo]:
        """Download HFED datasets.

        Returns:
            ResourceInfo objects which point a resource name to a file path.
        """
        resources = []
        cache_dir = os.environ.get("CODERS_CACHE_DIR")
        for resource_name, url in self.resource_urls.items():
            if cache_dir:
                of = fsspec.open(
                    f"filecache::{url}", "rb", filecache={"cache_storage": cache_dir}
                )
                with of as f:
                    bytes = io.BytesIO(f.read())
            else:
                bytes = io.BytesIO(requests.get(url).content)
            resource_filename = f"{resource_name}.csv"
            resource_path = self.download_directory.joinpath(resource_filename)
            resource_path.write_bytes(bytes.getvalue())
            resources.append(ResourceInfo(name=resource_name, path=resource_path))
        return resources
