from dagster import Definitions, define_asset_job, load_assets_from_modules

from emh_catalyst_001.io_managers import VersionedMySQLIOManager
from emh_catalyst_001.landing import assets
from emh_catalyst_001.utils import get_mysql_url

all_assets = load_assets_from_modules([assets], group_name="landing")

defs = Definitions(
    jobs=[define_asset_job(name="coders_landing_job")],
    assets=all_assets,
    resources={
        "versioned_mysql_io_manager": VersionedMySQLIOManager(get_mysql_url()),
    },
)
