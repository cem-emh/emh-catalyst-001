"""Classes used for creating archives."""

from abc import ABC, abstractmethod
import io
from pathlib import Path
import tempfile
import logging
import uuid

from frictionless import Package, Resource
from pydantic import BaseModel
from swiftclient.service import SwiftUploadObject
import pandas as pd
import sqlalchemy as sa

from emh_catalyst_001.utils import SwiftEngine, get_mysql_url

logger = logging.getLogger()


class ResourceInfo(BaseModel):
    """
    Class for storing information about a resource.

    Attributes:
        name: The name of the resource.
        path: The path to the resource file. The file
            is typically saved in the temporary download directory.
    """

    name: str
    path: Path


class AbstractDatasetArchiver(ABC):
    """ "
    Abstract class for archiving datasets.

    To create a new archiver, subclass this class and implement the
    get_resources method. The ``package`` attribute should be set to
    a valid frictionless Package object.

    Attributes:
        package: A frictionless Package object that contains metadata
            about the dataset.
        download_directory: A temporary directory where the raw data
            can be downloaded.
        swift_engine: A SwiftEngine object that is used to upload the
            archived data to a Swift container.
    """

    package: Package

    def __init__(self):
        """
        Constructs all of the necessary attributes for the archiver.
        """
        self.download_directory = tempfile.TemporaryDirectory()
        self.download_directory = Path(self.download_directory.name)
        self.download_directory.mkdir(exist_ok=True, parents=True)

        self.swift_engine = SwiftEngine()

        self.mysql_engine = sa.create_engine(get_mysql_url())

    @abstractmethod
    def get_resources(self) -> list[ResourceInfo]:
        """Abstract method that contains logic for downloading resource data."""
        pass

    @classmethod
    def get_name(cls) -> str:
        """Get the name of the archiver from the package."""
        return cls.package.name

    def write_to_swift(self, archive_id: str, resources: list[ResourceInfo]) -> None:
        """Write the archive resources to Swift.

        Args:
            archive_id: the identifier for this version.
            resources: A generator that yields ResourceInfo objects to be archived.
        """
        # generate a UUID for the archive
        # TODO: Generate a DOI for the archive and add it to the package
        self.package.archive_version = archive_id
        self.package.released_at = pd.Timestamp.now().isoformat()
        self.package.accessed_at = pd.Timestamp.now().isoformat()
        for resource in resources:
            # get file extension from path
            file_extension = resource.path.suffix
            object_name = (
                f"{self.package.name}/{archive_id}/{resource.name}{file_extension}"
            )
            self.swift_engine.upload(
                "coders-archive",
                [SwiftUploadObject(source=str(resource.path), object_name=object_name)],
            )
            # add the resource to the package
            # TODO: figure out how to store and add more resource metadata
            resource_path = f"{resource.name}{file_extension}"
            self.package.add_resource(Resource(path=resource_path, name=resource.name))

        # add the package to the archive
        object_name = f"{self.package.name}/{archive_id}/datapackage.json"
        logger.debug(f"Uploading {object_name}")
        self.swift_engine.upload(
            "coders-archive",
            [
                SwiftUploadObject(
                    source=io.StringIO(self.package.to_json()), object_name=object_name
                )
            ],
        )

    def load_metadata_to_mysql(self):
        """Load the package metadata to MySQL."""
        # TODO: Load 1:m tables, licenses and sources to MySQL
        package_df = pd.DataFrame(
            [
                {
                    "archive_version": self.package.archive_version,
                    "data_source_id": self.package.data_source_id,
                    "released_at": self.package.accessed_at,
                    "accessed_at": self.package.accessed_at,
                }
            ]
        )
        package_df.to_sql(
            "archive_metadata", self.mysql_engine, if_exists="append", index=False
        )

    def execute(self):
        """Method that contains logic for archiving resource data."""
        # get the resource data
        resources = self.get_resources()
        self.write_to_swift("latest", resources)
        self.write_to_swift(str(uuid.uuid4()), resources)
        self.load_metadata_to_mysql()
