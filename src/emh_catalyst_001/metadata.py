from frictionless import Package, Resource
import json
import pandas as pd


class CODERSResource(Resource):
    def __init__(
        self,
        *args,
        author: str,
        subject_area: str,
        data_format: str,
        access_conditions: str,
        **kwargs,
    ):
        """Initialize the CODERSPackage object."""
        # TODO: I want to add everything in emh_catalyst_001.schema to the schema attribute
        # but I think it will be an implementation rabit hole so i'm going to keep them separate for now.
        super().__init__(*args, **kwargs)
        self.author = author
        self.subject_area = subject_area
        self.data_format = data_format
        self.access_conditions = access_conditions

    def to_dict(self) -> dict:
        dct = super().to_dict()
        dct["author"] = self.author
        dct["subject_area"] = self.subject_area
        dct["data_format"] = self.data_format
        dct["access_conditions"] = self.access_conditions
        return dct


class CODERSPackage(Package):
    """Frictionless Package with additional attributes for CODERS metadata."""

    def __init__(
        self,
        *args,
        data_type: str = None,
        subject_area: str = None,
        released_at: str = None,
        accessed_at: str = None,
        doi: str = None,
        citation_information: str = None,
        update_frequency: str = None,
        archive_version: str = None,
        data_source_id: str = None,
        **kwargs,
    ):
        """Initialize the CODERSPackage object."""
        super().__init__(*args, **kwargs)
        self.archive_version = archive_version
        self.data_type = data_type
        self.subject_area = subject_area
        self.released_at = released_at
        self.accessed_at = accessed_at
        self.doi = doi
        self.citation_information = citation_information
        self.update_frequency = update_frequency
        self.data_source_id = data_source_id
        # TODO: Add validation methods for these attributes

    def to_dict(self) -> dict:
        dct = super().to_dict()
        dct["data_type"] = self.data_type
        dct["subject_area"] = self.subject_area
        dct["released_at"] = self.released_at
        dct["accessed_at"] = self.accessed_at
        dct["doi"] = self.doi
        dct["citation_information"] = self.citation_information
        dct["update_frequency"] = self.update_frequency
        dct["archive_version"] = self.archive_version
        dct["data_source_id"] = self.data_source_id
        return dct

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    def to_dataframes(self) -> dict[str, pd.DataFrame]:
        dfs = {}
        dfs["package"] = pd.DataFrame()

        dct = self.to_dict()
        dct["keywords"] = ", ".join(dct["keywords"])

        for key, value in dct.items():
            if isinstance(value, list):
                dfs[key] = pd.DataFrame(value)
            else:
                dfs["package"][key] = pd.Series(value)
        return dfs


# TODO 2024-09-24: why not just instantiate the CODERSResource classes directly here?
RESOURCES = {
    "hfed_demand": {
        "author": "EMH",
        "subject_area": "Electricity Demand",
        "data_format": "MySQL table",
        "access_conditions": "CODERS terms",
        "description": "Hourly electricity demand data for Newfoundland and Quebec.",
    },
    "data_source": {
        "author": "EMH",
        "subject_area": "Metadata",
        "data_format": "MySQL table",
        "access_conditions": "CODERS terms",
        "description": "Information about each data source organization.",
    },
}
# TODO: flesh out coders package
CODERS_PACKAGE = CODERSPackage(
    name="CODERS",
    description="CODERS database.",
)
for resource_name, resource_metadata in RESOURCES.items():
    CODERS_PACKAGE.add_resource(
        CODERSResource(
            name=resource_name,
            author=resource_metadata["author"],
            subject_area=resource_metadata["subject_area"],
            data_format=resource_metadata["data_format"],
            access_conditions=resource_metadata["access_conditions"],
            description=resource_metadata["description"],
        )
    )
