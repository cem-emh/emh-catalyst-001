import datetime
import logging

from dagster import IOManager, OutputContext, InputContext
import pandas as pd
import sqlalchemy as sa
import git

from emh_catalyst_001.metadata import CODERS_PACKAGE, CODERSPackage
from emh_catalyst_001.schema import asset_version, coders_version
from emh_catalyst_001.utils import (
    VersionedTable,
)


logger = logging.getLogger(__name__)


class VersionedMySQLIOManager(IOManager):
    """
    IO manager for *versioned* data tables that live in MySQL.

    Use for all data tables you want to persist as part of CODERS, so the table
    versions are correctly persisted and retrieved.
    """

    def __init__(self, sql_url: str, package: CODERSPackage = CODERS_PACKAGE):
        """Constructor.

        Arguments:
            sql_url: entire SQLAlchemy URI for the database connection.
        """
        self.engine = sa.create_engine(sql_url)
        self.package = package

    def load_input(self, context: InputContext) -> VersionedTable:
        """Read in a specific asset value.

        Called by Dagster with a specific InputContext.

        When reading from a fact table, only reads the facts associated with an
        `asset_version` that is marked as active. This means that downstream
        assets can then track the versions of their input assets in their
        return values.
        """
        table_name = "_".join(context.get_asset_identifier())
        fact_query = (
            sa.select(sa.text(f"{table_name}.*"))
            .join_from(
                sa.text(table_name),
                asset_version,
                sa.text(f"{table_name}.asset_version") == asset_version.c.id,
            )
            .where(asset_version.c.active)
        )

        with self.engine.begin() as conn:
            active_version_query = (
                sa.select(asset_version.c.id)
                .where(asset_version.c.active)
                .where(asset_version.c.asset_name == table_name)
            )
            active_versions = conn.execute(active_version_query).fetchall()
            assert len(active_versions) == 1, (
                f"There were {len(active_versions)} active versions for {table_name}"
            )
            active_version = active_versions[0][0]

            dataframe = pd.read_sql_query(sql=fact_query, con=conn)
            return VersionedTable(dataframe=dataframe, version_id=active_version)

    def _get_git_hash(self) -> str:
        """Get the current Git commit hash."""
        repo = git.Repo(search_parent_directories=True)
        return repo.head.object.hexsha

    def __update_coders_version(self, conn: sa.Connection, run_id: str) -> str:
        res = conn.execute(
            sa.select(coders_version.c.coders_version).where(
                coders_version.c.run_id == run_id
            )
        ).fetchall()
        if len(res) > 1:
            raise ValueError(f"More than one coders version found for {run_id}: {res}")
        if len(res) == 1:
            return res[0][0]

        now = datetime.datetime.now(datetime.UTC)
        num_versions_today = conn.execute(
            sa.select(sa.func.count())
            .select_from(coders_version)
            .where(
                coders_version.c.date_created
                >= datetime.datetime(now.year, now.month, now.day, tzinfo=datetime.UTC)
            )
            .where(coders_version.c.date_created < now)
        ).fetchall()[0][0]
        new_iteration = num_versions_today
        version = f"{now.year:04}.{now.month:02}.{now.day:02}.{new_iteration:02}"

        conn.execute(
            sa.insert(coders_version).values(
                run_id=run_id, date_created=now, coders_version=version
            )
        )

    def __update_asset_version(
        self,
        conn: sa.Connection,
        table_name: str,
        asset_version_id: str,
        run_id: str,
    ) -> None:
        """Generate asset version metadata and update active status.

        * create a new asset version with all the appropriate metadata fields.
        * make sure all other versions for this base table name are deactivated
          so there is only one version of the table getting read in `load_input`.
        """

        new_asset_version = sa.insert(asset_version).values(
            id=asset_version_id,
            active=True,
            git_hash=self._get_git_hash(),
            asset_name=table_name,
            run_id=run_id,
        )

        deactivate_old_versions = (
            sa.update(asset_version)
            .where(asset_version.c.asset_name == table_name)
            .values(active=False)
        )

        conn.execute(deactivate_old_versions)
        conn.execute(new_asset_version)

    def __fast_to_sql(
        self,
        con: sa.Connection,
        table_name: str,
        dataframe: pd.DataFrame,
    ) -> None:
        """Insert giant tables to MySQL, as quickly as we can.

        For insert performance: also disable foreign key checks. see
        https://dev.mysql.com/doc/refman/8.4/en/optimizing-innodb-bulk-data-loading.html

        If performance becomes more of a problem, consider writing a custom
        function to pass to `method` - using LOAD DATA LOCAL INFILE
        https://dev.mysql.com/doc/refman/8.4/en/load-data.html . We'd have to
        enable local file loading on the MySQL instance, though.

        This bypasses the Python type conversion step that is taking so long in to_sql.
        """

        con.exec_driver_sql("SET foreign_key_checks=0;")
        logger.info(f"writing {len(dataframe)} rows to {table_name}")
        dataframe.to_sql(
            name=table_name,
            con=con,
            if_exists="append",
            index=False,
            chunksize=1_000,
            method="multi",
        )
        con.exec_driver_sql("SET foreign_key_checks=1;")

    def handle_output(self, context: OutputContext, asset: pd.DataFrame) -> None:
        """Entry point for persisting asset outputs.

        At a high level, this persists the asset outputs, the asset version
        metadata, and associates the Dagster run ID with a human-readable ID.

        Finally, this makes sure that the new asset version is marked as
        'active' and the old ones are not.
        """
        table_name = "_".join(context.get_asset_identifier())
        asset_version_id = f"{table_name}__{context.run_id}"

        asset["asset_version"] = asset_version_id

        with self.engine.begin() as conn:
            self.__update_coders_version(conn=conn, run_id=context.run_id)
            self.__fast_to_sql(
                con=conn,
                table_name=table_name,
                dataframe=asset,
            )
            self.__update_asset_version(
                conn=conn,
                table_name=table_name,
                asset_version_id=asset_version_id,
                run_id=context.run_id,
            )
