import datetime

import sqlalchemy as sa

metadata = sa.MetaData()

uuid_length = 37

asset_version_length = uuid_length + 64

hfed_demand_fact = sa.Table(
    "hfed_demand",
    metadata,
    sa.Column("id", sa.Integer, autoincrement=True, primary_key=True),
    sa.Column("dataflow", sa.String(255)),
    sa.Column("freq", sa.String(8)),
    sa.Column("ref_area", sa.String(20)),
    sa.Column("counterpart_area", sa.String(20)),
    sa.Column("energy_flows", sa.String(255)),
    sa.Column("time_period", sa.DateTime),
    sa.Column("obs_value", sa.Float),
    sa.Column("datetime_local", sa.DateTime),
    sa.Column("daylight_offset", sa.Float),
    sa.Column("datetime_local_offset", sa.Float),
    sa.Column("unit_measure", sa.String(20)),
    sa.Column("unit_mult", sa.Float),
    sa.Column("measure_type", sa.String(20)),
    sa.Column("obs_status", sa.String(8)),
    sa.Column("conf_status", sa.String(8)),
    sa.Column("quality", sa.JSON(), nullable=True),
    sa.Column(
        "archive_version_id",
        sa.String(255),
        sa.ForeignKey("archive_metadata.archive_version"),
    ),
    sa.Column("data_source_id", sa.String(255), sa.ForeignKey("data_source.id")),
    sa.Column(
        "asset_version",
        sa.String(asset_version_length),
        sa.ForeignKey("asset_version.id"),
        index=True,
        autoincrement=False,
    ),
    sa.UniqueConstraint("ref_area", "time_period", "asset_version"),
)

data_source = sa.Table(
    "data_source",
    metadata,
    sa.Column(
        "id",
        sa.String(50),
        primary_key=True,
        comment="unique string identifying this data source - combined with asset_version",
    ),
    sa.Column(
        "authors",
        sa.String(255),
        comment="comma-separated names of authors or organizations.",
    ),
    sa.Column(
        "name",
        sa.String(255),
        comment="reader-facing name of this source - what you would put in the citation.",
    ),
    sa.Column("description", sa.Text, comment="description of the data source."),
    sa.Column(
        "cited_part",
        sa.String(255),
        comment="The location of the information you cited, within the source (e.g. page number, table number, etc.)",
    ),
    sa.Column(
        "doi",
        sa.String(255),
        comment="The DOI of the source, if available - e.g. 10.5281/zenodo.4127043",
    ),
    sa.Column("url", sa.String(255), comment="The URL of the source, if available"),
    sa.Column(
        "published_at",
        sa.DateTime(),
        comment="The date, and potentially the time, this data source was updated, if available. For continuously updated data sources, refer to the archive_metadata's accessed_at field.",
    ),
)

# archive_metadata, each row is an archive version, columns are everything in package
archive_metadata = sa.Table(
    "archive_metadata",
    metadata,
    sa.Column("archive_version", sa.String(225), primary_key=True, autoincrement=False),
    sa.Column("released_at", sa.DateTime()),
    sa.Column("accessed_at", sa.DateTime()),
    sa.Column("data_source_id", sa.String(50), sa.ForeignKey("data_source.id")),
)

asset_version = sa.Table(
    "asset_version",
    metadata,
    sa.Column(
        "id", sa.String(asset_version_length), primary_key=True, autoincrement=False
    ),
    sa.Column("active", sa.Boolean(), index=True),
    sa.Column("git_hash", sa.String(255)),
    sa.Column("asset_name", sa.String(255)),
    sa.Column("run_id", sa.String(uuid_length)),
    sa.Column("date_created", sa.DateTime, default=datetime.datetime.now),
    sa.Column("access_conditions", sa.String(255)),
)


coders_version = sa.Table(
    "coders_version",
    metadata,
    sa.Column(
        "run_id",
        sa.String(uuid_length),
        primary_key=True,
    ),
    sa.Column("date_created", sa.DateTime, default=datetime.datetime.now),
    sa.Column("coders_version", sa.String(len("YYYY.MM.DD.XX"))),
)
