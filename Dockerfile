FROM python:3.13-slim

ENV REPO=/tmp/dagster/repo
ENV ENV_NAME=base
ENV DAGSTER_HOME=/tmp/dagster/dagster_home/
ENV CODERS_CACHE_DIR=/tmp/cache

# Install some apt-get packages
# hadolint ignore=DL3008
RUN apt-get update -y --no-install-recommends && \
    apt-get install -y --no-install-recommends git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --no-cache-dir uv==0.5.8

RUN mkdir -p ${DAGSTER_HOME} ${REPO} ${CODERS_CACHE_DIR}
WORKDIR ${REPO}

COPY . .
RUN mkdir -p _build && \
    uv sync

# Copy dagster instance YAML to $DAGSTER_HOME
# RUN ln -s ${REPO}/dagster.yaml $DAGSTER_HOME

# add the repo as a safe git directory
RUN git config --global --add safe.directory /tmp/dagster/repo

EXPOSE 3000

CMD ["uv", "run", "dagster-webserver", "-h", "0.0.0.0", "-p", "3000", "-m", "emh_catalyst_001", "-m", "emh_catalyst_001.landing"]
