.PHONY:
mysql_shell:
	docker exec -it emh_catalyst-mysql-1 mysql -u root -p

.PHONY:
dagster_shell:
	docker exec -it emh_catalyst-dagster-1 bash

.PHONY:
unit_tests:
	docker-compose up -d && docker-compose exec -T dagster uv run pytest tests/unit

.PHONY:
integration_tests:
	docker-compose up -d && docker-compose exec dagster uv run pytest tests/integration $(ARGS)
