import sqlalchemy as sa

from dagster import build_input_context, build_output_context, AssetKey

import pandas as pd
import pytest

from emh_catalyst_001.io_managers import VersionedMySQLIOManager
from emh_catalyst_001.schema import metadata
from emh_catalyst_001.metadata import CODERSPackage, CODERSResource


@pytest.fixture
def io_manager(coders_package):
    test_db = "mysql+mysqlconnector://coders-test:example@mysql-test:3306/coders-test"
    engine = sa.create_engine(test_db)
    existing_metadata = sa.MetaData()
    existing_metadata.reflect(engine)
    existing_metadata.drop_all(engine)
    metadata.create_all(engine)

    return VersionedMySQLIOManager(
        sql_url=test_db,
        package=coders_package,
    )


def __make_resource(name: str, **kwargs) -> CODERSResource:
    default_args = {
        "author": "emh",
        "subject_area": "test subject",
        "data_format": "sql",
        "access_conditions": "open",
        "description": "test asset with no restrictions.",
    }
    return CODERSResource(name=name, **(default_args | kwargs))


@pytest.fixture
def coders_package():
    return CODERSPackage(
        name="CODERS",
        description="CODERS database.",
        resources=[
            __make_resource("test_asset"),
            __make_resource("test_child_asset"),
            __make_resource("test_parent_asset"),
        ],
    )


def test_versioned_table_roundtrip(io_manager):
    asset_key = AssetKey("test_asset")

    out_table = pd.DataFrame({"test_id": [1], "test_fact": [10]})
    output_context = build_output_context(asset_key=asset_key, run_id="test_run")
    io_manager.handle_output(output_context, out_table)

    input_context = build_input_context(asset_key=asset_key)
    in_table = io_manager.load_input(input_context)

    assert in_table.version_id == "test_asset__test_run"
    assert in_table.dataframe.equals(out_table)


def test_versioned_table_updates_to_newest_version(io_manager):
    asset_key = AssetKey("test_asset")

    old_out_table = pd.DataFrame({"test_id": [1], "test_fact": [10]})
    old_output_context = build_output_context(
        asset_key=asset_key, run_id="old_test_run"
    )
    io_manager.handle_output(old_output_context, old_out_table)

    new_out_table = pd.DataFrame({"test_id": [1], "test_fact": [11]})
    new_output_context = build_output_context(
        asset_key=asset_key, run_id="new_test_run"
    )
    io_manager.handle_output(new_output_context, new_out_table)

    input_context = build_input_context(asset_key=asset_key)
    in_table = io_manager.load_input(input_context)

    assert in_table.version_id == "test_asset__new_test_run"
    assert in_table.dataframe.equals(new_out_table)
