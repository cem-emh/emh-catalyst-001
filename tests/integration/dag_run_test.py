import logging
import subprocess

from alembic.autogenerate import compare_metadata
from alembic.migration import MigrationContext
from dagster import RunConfig
import sqlalchemy as sa

from emh_catalyst_001 import defs, io_managers
from emh_catalyst_001.schema import metadata, coders_version
from emh_catalyst_001.raw.core import LandingZoneArchiveVersions
from emh_catalyst_001.utils import get_mysql_url
import emh_catalyst_001.landing


logger = logging.getLogger(__name__)


def test_alembic_migrations(monkeypatch):
    test_db = "mysql+mysqlconnector://coders-test:example@mysql-test:3306/coders-test"
    engine = sa.create_engine(test_db)
    existing_metadata = sa.MetaData()
    existing_metadata.reflect(engine)
    existing_metadata.drop_all(engine)
    monkeypatch.setenv("MYSQL_URL", test_db)
    subprocess.check_call(["alembic", "upgrade", "head"])

    engine = sa.create_engine(test_db)
    mc = MigrationContext.configure(engine.connect())
    diff = compare_metadata(mc, metadata)

    assert len(diff) == 0


def test_dag_run(monkeypatch):
    test_db = "mysql+mysqlconnector://coders-test:example@mysql-test:3306/coders-test"
    engine = sa.create_engine(test_db)
    existing_metadata = sa.MetaData()
    existing_metadata.reflect(engine)
    existing_metadata.drop_all(engine)
    engine = sa.create_engine(test_db)
    monkeypatch.setenv("MYSQL_URL", test_db)
    metadata.create_all(engine)

    logger.info("Running landing job...")
    landing_job = emh_catalyst_001.landing.defs.get_job_def("coders_landing_job")
    landing_job.execute_in_process()

    logger.info("Running ETL job pinned to `latest` archive versions...")
    job = defs.get_job_def("coders_job")
    job.execute_in_process(
        run_config=RunConfig(
            resources={
                "landing_zone_archive_versions": LandingZoneArchiveVersions(
                    hfed="latest"
                ),
            }
        ),
        resources={
            "versioned_mysql_io_manager": io_managers.VersionedMySQLIOManager(
                get_mysql_url()
            ),
        },
    )

    with sa.create_engine(get_mysql_url()).connect() as conn:
        res = conn.execute(sa.select(coders_version)).fetchall()
        assert len(res) == 1
        assert res[0].coders_version.split(".")[-1] == "00"
