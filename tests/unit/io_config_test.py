from emh_catalyst_001 import create_defs
from emh_catalyst_001.utils import SwiftEngine


def test_mysql_url_with_env(monkeypatch):
    mysql_url = "mysql+mysqlconnector://test:test@mysql:3306/test"
    monkeypatch.setenv(
        "MYSQL_URL",
        mysql_url,
    )
    defs = create_defs()

    assert (
        defs.resources["versioned_mysql_io_manager"].engine.url.render_as_string(
            hide_password=False
        )
        == mysql_url
    )


def test_mysql_url_with_no_env():
    mysql_url = "mysql+mysqlconnector://coders-dev:example@mysql:3306/coders-dev"
    defs = create_defs()
    assert (
        defs.resources["versioned_mysql_io_manager"].engine.url.render_as_string(
            hide_password=False
        )
        == mysql_url
    )


def test_swift_manager_with_env(monkeypatch):
    base_url = "test_base"
    user = "new_test_user"
    password = "new_test_password"
    key = "new_test_key"
    monkeypatch.setenv("SWIFT_BASE_URL", base_url)
    monkeypatch.setenv("SWIFT_USER", user)
    monkeypatch.setenv("SWIFT_PASSWORD", password)
    monkeypatch.setenv("SWIFT_KEY", key)
    expected_options = {
        "auth_version": "1.0",
        "user": f"{user}:{password}",
        "key": f"{key}",
        "auth": f"{base_url}/auth/v1.0",
    }
    engine = SwiftEngine()
    for key, value in expected_options.items():
        assert engine.service._options[key] == value


def test_swift_manager_with_no_env():
    expected_options = {
        "auth_version": "1.0",
        "user": "test:tester",
        "key": "testing",
        "auth": "http://swift:8080/auth/v1.0",
    }
    engine = SwiftEngine()
    for key, value in expected_options.items():
        assert engine.service._options[key] == value
