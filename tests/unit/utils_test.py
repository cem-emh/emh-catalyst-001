"""Test the utils module."""

import pytest

from emh_catalyst_001.utils import create_uuid_from_values


def test_create_uuid_from_values_raises_error():
    """Test that create_uuid_from_values raises an error when values is not a list or string series."""
    with pytest.raises(ValueError):
        create_uuid_from_values(123)


def test_create_uuid_from_values():
    """Test that create_uuid_from_values returns the expected hash for a value."""
    value = ["a", "b", "c"]
    assert len(create_uuid_from_values(value)) == 32
