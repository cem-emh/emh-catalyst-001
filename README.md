# EMH-Catalyst-001


## Introduction
Welcome to the CODERS ETL!

This is an ETL pipeline that:

* archives Canadian energy data from the Internet
* transforms the archived data into a uniform, clean, tabular format
* loads the transformed data into the CODERS MySQL database

The outputs of this are meant to be accessed through the CODERS webservice.


## Getting the code

The first thing you're going to need to do is get the source code for CODERS.
[Clone the repository](https://help.github.com/articles/cloning-a-repository/)
with this command:

```bash
$ git clone git@gitlab.com:cem-emh/emh-catalyst-001.git
```

This will download the whole history of the project, including the most recent version, to a directory called `emh-catalyst-001`.

## Running the code

Next, you'll need to run the code and make sure everything works as expected.

We've set up a Docker Compose environment, which lets you run the entire system on your local computer. Here's how to use that.

1. Install Docker Desktop, for [Mac](https://docs.docker.com/desktop/install/mac-install/), [Windows](https://docs.docker.com/desktop/install/windows-install/), or [Linux](https://docs.docker.com/desktop/install/linux/).

2. Navigate to this repository's root directory, and run:
   ```bash
   $ docker compose build
   $ docker compose up
   ```
   This will start up several services at once:
   1. `dagster`, which runs the actual pipeline. It exposes a [web
      UI](https://docs.dagster.io/getting-started/quickstart#navigating-the-user-interface),
      which you can access in your browser at `localhost:3000`.
   2. `jupyter`, which runs a Jupyter Lab instance within the Docker environment, available at `localhost:8888`. Useful for exploration and debugging.
   3. `mysql`, a local copy of the database that we output data to.
   4. `mysql-test` - another database, but for integration testing purposes only.
   5. `swift`, a local version of the object store that we archive data files
      to. **Note that the Docker version of `swift` doesn't persist files when
      the container shuts down.**


Now, you should be able to navigate to `localhost:3000` and see the [Dagster
UI](https://docs.dagster.io/getting-started/quickstart#navigating-the-user-interface).


To see the [Jupyter
Lab
UI](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html), you'll need to get a special URL with an authentication token. To find that URL, run:

```bash
$ docker compose logs jupyter | grep "token"
```
Then copy and paste one of those URLs into your browser.

We've set up bind mounts, so any changes to `src`, `tests`, and `alembic` should
be immediately reflected within Dagster and Jupyter Lab. If you make changes to
`pyproject.toml`, however, you will have to re-build the image with `docker
compose build`.

### Integration tests

To make sure all the code is working as expected, we have also written some integration tests. Run these with `make integration_tests`:

```bash
$ make integration_tests
docker-compose up -d && docker-compose exec dagster micromamba run pytest tests/integration --log-cli-level=DEBUG
WARN[0000] /Users/dazhong-catalyst/work/emh-catalyst-001/docker-compose.yml: `version` is obsolete
[+] Running 5/0
 ✔ Container emh_catalyst-mysql-test-1  Running                                                                                                                                            0.0s
 ✔ Container emh_catalyst-dagster-1     Running                                                                                                                                            0.0s
 ✔ Container emh_catalyst-swift-1       Running                                                                                                                                            0.0s
 ✔ Container emh_catalyst-jupyter-1     Running                                                                                                                                            0.0s
 ✔ Container emh_catalyst-mysql-1       Running                                                                                                                                            0.0s
WARN[0000] /Users/dazhong-catalyst/work/emh-catalyst-001/docker-compose.yml: `version` is obsolete
===================================================================================== test session starts ======================================================================================
platform linux -- Python 3.12.5, pytest-8.3.2, pluggy-1.5.0
rootdir: /tmp/dagster/repo
configfile: pyproject.toml
plugins: anyio-4.4.0, time-machine-2.15.0
collecting ...
<rest of output cut for brevity>
```

This will take several minutes to complete.

### Triggering manual ETL runs

Now that the tests pass, you might want to make some changes and trigger manual ETL runs where you can poke and prod at the data.

See [Running the ETL manually](running-the-etl-manually.md) for details.

## Editing the code

Finally, you'll need to set up a local Python environment so you can edit the code. We use the `mamba` and `conda-lock` tools together to manage the environment.

You'll also need to set up some checks to make sure your code is in good shape when you commit. We use `pre-commit` to manage these.

So we'll:

1. Install `mamba` and `conda-lock` to manage the environment
2. Use those tools to install all the dependencies & the ETL code itself within the `coders-dev` environment
3. Set up pre-commit checks.

### mamba & conda-lock

`mamba` allows us to create isolated Python environments & install packages into them; `conda-lock` allows us to specify exactly which packages to install. We need both.

To install `mamba`, we recommend using [miniforge](https://github.com/conda-forge/miniforge).

Once you've installed `mamba`, you should be able to use it to activate & deactivate environments:

```bash
$ mamba deactivate
$ mamba activate base
$ mamba info | grep "active environment"
     active environment : base
```

To install conda lock:

```
mamba run --name base mamba install --quiet --yes "conda-lock>=2.5.7"
```

You should be able to run `conda-lock --help` from within the `base` environment at this point:

```bash
$ mamba activate base
$ conda-lock --help
```

### Set up the actual environment

Now that we have the tools, we can install all the dependencies and the CODERS ETL itself!

To create the dev environment, run:

```bash
$ mamba run --name base conda-lock install --name coders-dev environments/conda-lock.yml
$ mamba activate coders-dev
$ pip install -e .
```

At this point you should be able to import `emh_catalyst_001` within Python:

```bash
$ python                                                                                                                      (coders-dev)
Python 3.12.4 | packaged by conda-forge | (main, Jun 17 2024, 10:13:44) [Clang 16.0.6 ] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import emh_catalyst_001
```

You should also be able to turn these docs into a nice-looking HTML page with:

```
$ mkdocs serve # then go to localhost:8000 in your browser!
```

If you change a dependency in `pyproject.toml`, you can relock the conda lock files by running these commands:

```bash
$ rm environments/conda-lock.yml
$ conda-lock --mamba --file=pyproject.toml --lockfile=environments/conda-lock.yml
```

### pre-commit checks

Pre-commit checks are run when you try to make a new commit. These scripts can review your code and identify bugs, formatting errors, bad coding habits, and other issues before the code gets checked in. This gives you the opportunity to fix those issues before publishing them.

To make sure they are run before you commit any code, you need to enable the [pre-commit hooks](https://pre-commit.com/) with this command:

```
pre-commit install
```
# Command List to Deploy the Code Base


## verify docker builds,images,volumes and containers are empty.

### $ docker compose build
### $ docker compose down
### $ docker compose up
### $ docker compose logs jupyter | grep "token"
### $ docker compose exec dagster bash
### root@08e41aca2b91:/tmp/dagster/repo# uv run alembic upgrade head

### Then start running Dagster and Jupyter